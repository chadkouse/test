package main

const DEFAULT_BUCKET = "hale-tractor-863.appspot.com"
const DEFAULT_BUCKET_URL_PREFIX = "//storage.googleapis.com/hale-tractor-863.appspot.com/"
const USER_PHOTOS_PREFIX = "media/userphotos/"
