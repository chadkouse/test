package main

import (
	"appengine"
	"appengine/datastore"
	"appengine/taskqueue"
	"appengine/urlfetch"
	"errors"
	"fmt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/cloud"
	"google.golang.org/cloud/storage"
	"io"
	"mime/multipart"
	"net/http"
)

var (
	ErrNoSuchPerson = errors.New("person not found")
)

type Person struct {
	Id           int64
	Name         string
	Age          int
	Gender       string
	PhotoVersion int `json:"-"`
}

func (p *Person) GetKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Person", "", p.Id, nil)
}

func (p *Person) Create(c appengine.Context) (*datastore.Key, error) {

	k := datastore.NewIncompleteKey(c, "Person", nil)

	newKey, err := datastore.Put(c, k, p)
	p.Id = newKey.IntID()

	return newKey, err
}

func GetPersonById(c appengine.Context, id int64) (Person, error) {

	k := datastore.NewKey(c, "Person", "", id, nil)

	p := Person{}

	err := datastore.Get(c, k, &p)
	if err == datastore.ErrNoSuchEntity {
		err = ErrNoSuchPerson
		return p, err
	} else if err != nil {
		return p, err
	}
	p.Id = k.IntID()

	return p, nil

}

func (p *Person) Update(c appengine.Context) (*datastore.Key, error) {

	k := datastore.NewKey(c, "Person", "", p.Id, nil)

	return datastore.Put(c, k, p)

}

func (p *Person) Delete(c appengine.Context) error {

	return DeletePersonById(c, p.Id)
}

func DeletePersonById(c appengine.Context, id int64) error {
	k := datastore.NewKey(c, "Person", "", id, nil)
	return datastore.Delete(c, k)
}

func GetAllPersons(c appengine.Context, startKey string, limit int) (string, []Person, error) {

	q := datastore.NewQuery("Person")

	cursor, err := datastore.DecodeCursor(startKey)
	if err == nil {
		q = q.Start(cursor)
	}

	// Iterate over the results.
	persons := []Person{}

	q = q.Limit(limit)

	t := q.Run(c)
	keys, err := q.GetAll(c, &persons)
	if err != nil {
		return "", persons, err
	}

	for i := range keys {
		persons[i].Id = keys[i].IntID()
	}

	// Get updated cursor and store it for next time.
	if cursor, err := t.Cursor(); err == nil {
		return cursor.String(), persons, err
	}

	return "", persons, err

}

func (p *Person) StorePhoto(c appengine.Context, file multipart.File) error {

	hc := &http.Client{
		Transport: &oauth2.Transport{
			Source: google.AppEngineTokenSource(c, storage.ScopeFullControl),
			Base:   &urlfetch.Transport{Context: c},
		},
	}
	ctx := cloud.NewContext(appengine.AppID(c), hc)

	bucket := DEFAULT_BUCKET

	p.PhotoVersion++

	fileName := fmt.Sprintf("%v%v-photo-%v.png", USER_PHOTOS_PREFIX, p.Id, p.PhotoVersion)

	wc := storage.NewWriter(ctx, bucket, fileName)
	var maxFileSize int64
	maxFileSize = 10 * 1024 * 1024
	wc.ContentType = "image/png"
	defaultAcl, _ := storage.DefaultACL(ctx, bucket)
	wc.ACL = defaultAcl
	wc.ACL = append(wc.ACL, storage.ACLRule{storage.AllUsers, storage.RoleReader})
	written, err := io.CopyN(wc, file, maxFileSize+1)
	if err == io.EOF {
		err = nil
	}
	if err != nil {
		return err
	}
	if written >= maxFileSize {
		c.Infof("Denying profile photo upload for size limit")

		t2 := &taskqueue.Task{
			Path:   fmt.Sprintf("/tasks/person/%d/photo/%d", p.Id, p.PhotoVersion),
			Method: "DELETE",
			Name:   fmt.Sprintf("DELETEPHOTO-%d-%d", p.Id, p.PhotoVersion),
		}

		_, err := taskqueue.Add(c, t2, "")
		if err == taskqueue.ErrTaskAlreadyAdded {
			err = nil
		}
		if err != nil {
			return err
		}

		return fmt.Errorf("File was too large.  Must be under %v", maxFileSize)
	}

	if err = wc.Close(); err != nil {
		return err
	}

	if p.PhotoVersion > 1 {

		t := &taskqueue.Task{
			Path:   fmt.Sprintf("/tasks/person/%d/photo/%d", p.Id, p.PhotoVersion-1),
			Method: "DELETE",
			Name:   fmt.Sprintf("DELETEPHOTO-%d-%d", p.Id, p.PhotoVersion-1),
		}

		_, err := taskqueue.Add(c, t, "")
		if err == taskqueue.ErrTaskAlreadyAdded {
			err = nil
		}
		if err != nil {
			return err
		}
	}

	return nil

}

func DeletePersonPhoto(c appengine.Context, userId int64, version int) error {

	hc := &http.Client{
		Transport: &oauth2.Transport{
			Source: google.AppEngineTokenSource(c, storage.ScopeFullControl),
			Base:   &urlfetch.Transport{Context: c},
		},
	}
	ctx := cloud.NewContext(appengine.AppID(c), hc)

	bucket := DEFAULT_BUCKET

	oldFileName := fmt.Sprintf("%v%v-photo-%v.png", USER_PHOTOS_PREFIX, userId, version)
	return storage.DeleteObject(ctx, bucket, oldFileName)

}
