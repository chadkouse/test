package main

import (
	"appengine"
	"encoding/json"
	"fmt"
	"github.com/chadkouse/halgo"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

type ResourcePerson struct {
	halgo.Links
	halgo.Embedded
	Person
}

func GetResourcePerson(p Person) ResourcePerson {

	rp := ResourcePerson{
		Links: halgo.Links{}.Self(fmt.Sprintf("/person/%d", p.Id)).
			Add("p:uploadPhoto", halgo.Link{Href: fmt.Sprintf("/person/%d/photo", p.Id)}),
	}

	if p.PhotoVersion > 0 {
		rp.Links.Add("p:displayPhoto", halgo.Link{
			Href: fmt.Sprintf("%v%v%v-photo-%v.png", DEFAULT_BUCKET_URL_PREFIX, USER_PHOTOS_PREFIX, p.Id, p.PhotoVersion)})

	}

	rp.Person = p

	return rp
}

func CreatePersonHandler(rw http.ResponseWriter, req *http.Request) {
	decoder := json.NewDecoder(req.Body)
	p := new(Person)
	err := decoder.Decode(&p)
	if err != nil {
		fmt.Fprintf(rw, "Error %q", err)
		return
	}

	c := appengine.NewContext(req)

	_, err = p.Create(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Created new person %s", p)

}

func GetPersonHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	p, err := GetPersonById(c, id)

	if err == ErrNoSuchPerson {
		http.Error(rw, err.Error(), http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting person %v", err), http.StatusInternalServerError)
		return
	}

	rp := GetResourcePerson(p)

	js, err := json.Marshal(rp)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)
}

func UpdatePersonHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	p, err := GetPersonById(c, id)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting person %v", err), http.StatusInternalServerError)
		return
	}

	p.Name = "Stacey Kouse"
	p.Age = 34

	_, err = p.Update(c)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating person %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Updated person %s", p)

}

func DeletePersonHandler(rw http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	err = DeletePersonById(c, id)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error updating person %v", err), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(rw, "Deleted person %i", id)

}

func GetAllPersonHandler(rw http.ResponseWriter, req *http.Request) {

	startKey := req.FormValue("startKey")
	limit, err := strconv.Atoi(req.FormValue("limit"))
	if err != nil {
		limit = 10
	}

	c := appengine.NewContext(req)

	newStartKey, persons, err := GetAllPersons(c, startKey, limit)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error querying person %v", err), http.StatusInternalServerError)
		return
	}

	resourcePersons := []ResourcePerson{}

	for _, pv := range persons {
		resourcePersons = append(resourcePersons, GetResourcePerson(pv))
	}

	result := halgo.Resource{
		Embedded: halgo.Embedded{}.Add("persons", resourcePersons),
	}

	result.Links = halgo.Links{}.Self("/person")
	result.Links.Add("next", halgo.Link{Href: fmt.Sprintf("/person?startKey=%v&limit=%v", newStartKey, limit)})
	result.Links.Add("prev", halgo.Link{Href: fmt.Sprintf("/person?startKey=%v&limit=%v", startKey, limit)})

	js, err := json.Marshal(result)
	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(js)

}

func UploadPersonPhotoHandler(rw http.ResponseWriter, req *http.Request) {
	file, _, err := req.FormFile("photo")

	if err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	vars := mux.Vars(req)
	id, err := strconv.ParseInt(vars["id"], 10, 64)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing id %v", err), http.StatusInternalServerError)
		return
	}

	person, err := GetPersonById(c, id)
	if err != nil {
		http.Error(rw, fmt.Sprintf("Error getting person %v", err), http.StatusInternalServerError)
		return
	}

	err = person.StorePhoto(c, file)

	if err != nil {
		http.Error(rw, fmt.Sprintf("Error Uploading Photo %v", err), http.StatusInternalServerError)
		return
	}

	person.Update(c)

}

func DeletePersonPhotoHandler(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	userId, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing user id %v", err), http.StatusInternalServerError)
		return
	}

	photoVersion, err := strconv.Atoi(vars["photoVersion"])
	if err != nil {
		http.Error(rw, fmt.Sprintf("Error parsing photo version %v", err), http.StatusInternalServerError)
		return
	}

	c := appengine.NewContext(req)

	DeletePersonPhoto(c, userId, photoVersion)
}
