package main

import (
	"github.com/gorilla/mux"
	"net/http"
)

func init() {

	r := mux.NewRouter()
	r.HandleFunc("/person", CreatePersonHandler).Methods("POST", "PUT")
	r.HandleFunc("/person", GetAllPersonHandler).Methods("GET")
	r.HandleFunc("/person/{id}/photo", UploadPersonPhotoHandler).Methods("POST", "PUT")
	r.HandleFunc("/tasks/person/{id}/photo/{photoVersion}", DeletePersonPhotoHandler).Methods("DELETE")
	r.HandleFunc("/person/{id}", GetPersonHandler).Methods("GET")
	r.HandleFunc("/person/{id}", UpdatePersonHandler).Methods("POST", "PUT")
	r.HandleFunc("/person/{id}", DeletePersonHandler).Methods("DELETE")

	//    r.HandleFunc("/products", ProductsHandler)
	//    r.HandleFunc("/articles", ArticlesHandler)
	http.Handle("/", r)
}
